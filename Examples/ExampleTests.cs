﻿using Performance;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Examples
{
    public class ExampleTests
    {
        private readonly ITestOutputHelper _console;

        public ExampleTests(ITestOutputHelper console)
        {
            _console = console;
        }

        private Task<int> CopyInput(int input)
        {
            _console.WriteLine($"Thread ID: {Thread.CurrentThread.ManagedThreadId}.");
            Thread.Sleep(250);
            return Task.FromResult(input);
        }

        private Task<string> CopyInput(string input)
        {
            return Task<string>.Run(() =>
            {
                _console.WriteLine($"Thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                Thread.Sleep(250);
                return input;
            });
        }

        [Fact]
        public void NoAwait()
        {
            using StreamWriter file = new("../../../file.txt");
            new List<string>() { "abc", "efg", "hij", "klmn" }.ForEach(x => file.WriteLine(x));
        }

        [Fact]
        public async Task AwaitType1() // Ukazka 1
        {
            HttpResponseMessage response = await new HttpClient().GetAsync("http://google.com");
            _console.WriteLine($"Response code {response.StatusCode}.");
        }

        [Fact]
        public async Task AwaitType2()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            HttpResponseMessage response = await new HttpClient().GetAsync("http://google.com");
            await eratosthenes.PrimesParallel();

            _console.WriteLine($"Response code {response.StatusCode}.");
            eratosthenes.PrintLargestPrime();
            eratosthenes.PrintPrimes();
        }

        [Fact]
        public async Task AwaitType3() // Ukazka 5
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);
            HttpClient client = new HttpClient();

            Task task1 = eratosthenes.PrimesParallel();
            Task<HttpResponseMessage> task2 = client.GetAsync("http://google.com");

            await task1;
            HttpResponseMessage response = await task2;

            eratosthenes.PrintLargestPrime();
            _console.WriteLine($"Response code {response.StatusCode}.");
        }

        [Fact]
        public async Task AwaitType4() // Ukazka 2
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            
            Task<int> task1 = CopyInput(5);
            _console.WriteLine($"Task1 finished.");
            Task<int> task2 = CopyInput(6);
            _console.WriteLine($"Task2 finished.");
            Task<int> task3 = CopyInput(7);
            _console.WriteLine($"Task3 finished.");
            Task<int> task4 = CopyInput(8);
            _console.WriteLine($"Task4 finished.");

            int result = await task1 + await task2 + await task3 + await task4;
            _console.WriteLine($"Result is: {result}.");
        }

        [Fact]
        public void AwaitType5() // Ukazka 4 b,
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            
            string result = CopyInput(":D").Result;
            _console.WriteLine($"Task1 finished.");

            result += CopyInput(" :(").Result;
            _console.WriteLine($"Task2 finished.");

            _console.WriteLine($"Result is: {result}.");
        }

        [Fact]
        public void PrimesBenchmark()
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            eratosthenes.Primes();
            eratosthenes.PrintLargestPrime();
            eratosthenes.PrintPrimes();
        }

        [Fact]
        public async Task PrimesBenchmarkAsync() 
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            await eratosthenes.PrimesParallel();
            eratosthenes.PrintLargestPrime();
            eratosthenes.PrintPrimes();
        }

        [Fact]
        public void PrimesBenchmarkNotAsync() // Ukazka 4 a,
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            eratosthenes.PrimesParallel().Wait();
            eratosthenes.PrintLargestPrime();
            eratosthenes.PrintPrimes();
        }

        [Fact]
        public async Task PrimesParallel() // Ukazka 3
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            Task task1 = eratosthenes.PrimesParallel();
            _console.WriteLine("Task1 deployed.");

            Task task2 = eratosthenes.PrimesParallel();
            _console.WriteLine("Task2 deployed.");

            Task task3 = eratosthenes.PrimesParallel();
            _console.WriteLine("Task3 deployed.");

            Task task4 = eratosthenes.PrimesParallel();
            _console.WriteLine("Task4 deployed.");

            Task task5 = eratosthenes.PrimesParallel();
            _console.WriteLine("Task5 deployed.");

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;

            eratosthenes.PrintLargestPrime();
            eratosthenes.PrintPrimes();
        }

        [Fact]
        public async Task PrimesLooksParallel() // Ukazka 2
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            Task task1 = eratosthenes.PrimesNotParallel();
            _console.WriteLine("Task1 deployed.");

            Task task2 = eratosthenes.PrimesNotParallel();
            _console.WriteLine("Task2 deployed.");

            Task task3 = eratosthenes.PrimesNotParallel();
            _console.WriteLine("Task3 deployed.");

            Task task4 = eratosthenes.PrimesNotParallel();
            _console.WriteLine("Task4 deployed.");

            Task task5 = eratosthenes.PrimesNotParallel();
            _console.WriteLine("Task5 deployed.");

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;

            eratosthenes.PrintLargestPrime();
        }
        
        [Fact]
        public async Task PrimesNotParallel()
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            await eratosthenes.PrimesParallel();
            _console.WriteLine("Task1 deployed.");

            await eratosthenes.PrimesParallel();
            _console.WriteLine("Task2 deployed.");

            await eratosthenes.PrimesParallel();
            _console.WriteLine("Task3 deployed.");

            await eratosthenes.PrimesParallel();
            _console.WriteLine("Task4 deployed.");

            await eratosthenes.PrimesParallel();
            _console.WriteLine("Task5 deployed.");

            eratosthenes.PrintLargestPrime();
            eratosthenes.PrintPrimes();
        }

        [Fact]
        public async Task PrimesParallelSynchronized_1() // Princip ukazky 3
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            Task task1 = eratosthenes.PrimesParallelSynchronized_1();
            _console.WriteLine("Task1 deployed.");

            Task task2 = eratosthenes.PrimesParallelSynchronized_1();
            _console.WriteLine("Task2 deployed.");

            Task task3 = eratosthenes.PrimesParallelSynchronized_1();
            _console.WriteLine("Task3 deployed.");

            Task task4 = eratosthenes.PrimesParallelSynchronized_1();
            _console.WriteLine("Task4 deployed.");

            Task task5 = eratosthenes.PrimesParallelSynchronized_1();
            _console.WriteLine("Task5 deployed.");

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;

            eratosthenes.PrintLargestPrime();
            eratosthenes.PrintPrimes();
        }

        [Fact]
        public async Task PrimesParallelSynchronized_2() // Princip ukazky 3
        {
            _console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            Task task1 = eratosthenes.PrimesParallelSynchronized_2();
            _console.WriteLine("Task1 deployed.");

            Task task2 = eratosthenes.PrimesParallelSynchronized_2();
            _console.WriteLine("Task2 deployed.");

            Task task3 = eratosthenes.PrimesParallelSynchronized_2();
            _console.WriteLine("Task3 deployed.");

            Task task4 = eratosthenes.PrimesParallelSynchronized_2();
            _console.WriteLine("Task4 deployed.");

            Task task5 = eratosthenes.PrimesParallelSynchronized_2();
            _console.WriteLine("Task5 deployed.");

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;

            eratosthenes.PrintLargestPrime();
            eratosthenes.PrintPrimes();
        }

        [Theory]
        [InlineData(5)]
        [InlineData(10)]
        [InlineData(15)]
        [InlineData(20)]
        [InlineData(25)]
        [InlineData(30)]
        [InlineData(35)]
        [InlineData(40)]
        [InlineData(45)]
        [InlineData(50)]
        public async Task PrimesParallelMultiThreads(int threads)
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            Task[] tasks = new Task[threads];
            for (int i = 0; i < threads; i++)
            {
                tasks[i] = eratosthenes.PrimesParallel(i);
                _console.WriteLine($"Task{i + 1} deployed.");
            }

            for (int i = 0; i < threads; i++)
            {
                await tasks[i];
            }

            eratosthenes.PrintLargestPrime();
        }

        [Fact]
        public async Task DataRace()
        {
            int cycles = 10_000;
            int result = 0;

            Task task1 = Task.Run(() =>
            {
                for (int i = 0; i < cycles; i++)
                {
                    result++;
                }
            });

            Task task2 = Task.Run(() =>
            {
                for (int i = 0; i < cycles; i++)
                {
                    result--;
                }
            });

            await task1;
            await task2;

            Assert.Equal(0, result);
        }

        [Fact(Timeout = 1000)]
        public async Task DeadLock()
        {
            object lock1 = new object();
            object lock2 = new object();
            bool waitOver = false;
            bool dummy = false;

            Task task1 = Task.Run(() =>
            {
                lock (lock1)
                {
                    Thread.Sleep(5);
                    waitOver = true;

                    lock (lock2)
                    {
                        _ = 1 + 1;
                    }
                }
            });

            Task task2 = Task.Run(() =>
            {
                lock (lock2)
                {
                    Thread.Sleep(5);
                    dummy = true;

                    if (!waitOver)
                    {
                        lock (lock1)
                        {
                            _ = 1 + 1;
                        }
                    }
                }
            });

            await task1;
            await task2;

            Assert.True(dummy && waitOver);
        }

        [Fact]
        public void NewThreadsSynchronized_1()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            Thread thread1 = new Thread(() => 
            { 
                _console.WriteLine($"Thread1 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}."); 
                eratosthenes.PrimesSynchronized_1(); 
            });
            Thread thread2 = new Thread(() =>
            {
                _console.WriteLine($"Thread2 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
            });
            Thread thread3 = new Thread(() =>
            {
                _console.WriteLine($"Thread3 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
            });
            Thread thread4 = new Thread(() =>
            {
                _console.WriteLine($"Thread4 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
            });
            Thread thread5 = new Thread(() =>
            {
                _console.WriteLine($"Thread5 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
            });

            thread1.Start();
            _console.WriteLine("Thread1 deployed.");
            thread2.Start();
            _console.WriteLine("Thread2 deployed.");
            thread3.Start();
            _console.WriteLine("Thread3 deployed.");
            thread4.Start();
            _console.WriteLine("Thread4 deployed.");
            thread5.Start();
            _console.WriteLine("Thread5 deployed.");

            thread1.Join();
            _console.WriteLine("Thread1 finished.");
            thread2.Join();
            _console.WriteLine("Thread2 finished.");
            thread3.Join();
            _console.WriteLine("Thread3 finished.");
            thread4.Join();
            _console.WriteLine("Thread4 finished.");
            thread5.Join();
            _console.WriteLine("Thread5 finished.");

            eratosthenes.PrintLargestPrime();
        }

        [Fact]
        public void ThreadPoolThreadsSynchronized_1()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000, _console);

            CountdownEvent countdown = new CountdownEvent(5);

            ThreadPool.QueueUserWorkItem(new WaitCallback((_) => 
            {
                _console.WriteLine($"Thread1 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
                _console.WriteLine("Thread1 finished.");
            }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                _console.WriteLine($"Thread2 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
                _console.WriteLine("Thread2 finished.");
            }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                _console.WriteLine($"Thread3 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
                _console.WriteLine("Thread3 finished.");
            }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                _console.WriteLine($"Thread4 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
                _console.WriteLine("Thread4 finished.");
            }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                _console.WriteLine($"Thread5 starts on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
                _console.WriteLine("Thread5 finished.");
            }));

            countdown.Wait();
            eratosthenes.PrintLargestPrime();
        }
    }
}
