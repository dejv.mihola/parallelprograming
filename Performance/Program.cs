﻿using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Reports;
using BenchmarkDotNet.Running;
using System;
using System.IO;

namespace Performance
{
    class Program
    {
        static void Main(string[] args)
        {
            Summary summary = BenchmarkRunner.Run<PerformanceTests>();
            BenchmarkReportExporter.Default.ExportToLog(summary, ConsoleLogger.Default);

            // Spoustet jako release (ctr+F5), pro vygenerovani reportu spustit v PowerShellu prikazem:
            // 'dotnet run --project .\Performance\Performance.csproj --configuration Release'
        }
    }
}
