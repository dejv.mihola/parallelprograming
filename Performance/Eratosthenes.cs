﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Performance
{
    public class Eratosthenes
    {
        private volatile ulong[] _bits; // klicove slovo volatile oznamuje prekladaci, ze k temto datum muze pristupovat vice vlaken,
                                        // prekladac pote zajisti, aby optimalizece preskladanim jednotlivych instrukci, ktere lze provaded
                                        // u synchronniho kodu, nenarusily spravny prubeh pri soucasnem pristupu vice vlaken.
        private ulong _size;
        private ulong _sqrt_size;
        private const int _shift = 6; // 2^6 == 64
        private const ulong _mask = 0x3f; // & s 0x3f (63) == modulo 64, ale rychlejsi pro procesor, deleni je velmi narocne
        private ulong _currentLargestPrime = 1;
        private object _lock = new object();

        public Eratosthenes(ulong size)
        {
            if (size < 1)
            {
                throw new ArgumentException();
            }
            _size = size;
            _sqrt_size = (ulong)Math.Sqrt(_size);
            _bits = new ulong[((size - 1) >> _shift) + 1];
            _bits[0] = 0b11; // nastavi 0 a 1 jako 'prvocisla'
        }

        private readonly ITestOutputHelper _console;

        public Eratosthenes(ulong size, ITestOutputHelper console)
        {
            _console = console;

            if (size < 1)
            {
                throw new ArgumentException();
            }
            _size = size;
            _sqrt_size = (ulong)Math.Sqrt(_size);
            _bits = new ulong[((size - 1) >> _shift) + 1];
            _bits[0] = 0b11; // nastavi 0 a 1 jako 'prvocisla'
        }
        
        public void Primes()
        {
            #if LOG
                _console.WriteLine("Starting");
            #endif

            for (ulong k = 2; k < _sqrt_size; k++)
            {
                if ((_bits[k >> _shift] & (1UL << (int)(k & _mask))) == 0)
                {
                    #if DETAILED_LOG
                        _console.WriteLine($"Number: {k} on thread: {Thread.CurrentThread.ManagedThreadId}");
                    #endif
                    
                    ulong i = k * k;

                    while (i < _size)
                    {
                        _bits[i >> _shift] |= (1UL << (int)(i & _mask));
                        i += k;
                    }
                }
            }

            #if LOG
                _console.WriteLine("Finishing");
            #endif
        }

        public void PrimesSynchronized_1()
        {
            #if LOG
                _console.WriteLine("Starting");
            #endif

            ulong i, k = 1;

            while (k < _sqrt_size)
            {
                lock (_lock)
                {
                    for (k = _currentLargestPrime + 1; k < _sqrt_size; k++)
                    {
                        if (k > _currentLargestPrime && (_bits[k >> _shift] & (1UL << (int)(k & _mask))) == 0)
                        {
                            _currentLargestPrime = k;
                            break;
                        }
                    }
                }

                i = k * k;

                #if DETAILED_LOG
                    _console.WriteLine($"Number: {k} on thread: {Thread.CurrentThread.ManagedThreadId}");
                #endif

                while (i < _size)
                {
                    _bits[i >> _shift] |= 1UL << (int)(i & _mask);
                    i += k;
                }
            }

            #if LOG
                _console.WriteLine("Finishing");
            #endif
        }

        public void PrimesSynchronized_2()
        {
            #if LOG
                _console.WriteLine("Starting");
            #endif

            for (ulong k = 2; k < _sqrt_size; k++)
            {
                if (k > _currentLargestPrime && (_bits[k >> _shift] & (1UL << (int)(k & _mask))) == 0)
                {
                    lock(_lock)
                    {
                        if (k > _currentLargestPrime)
                        {
                            _currentLargestPrime = k;
                        }
                        else
                        {
                            k = _currentLargestPrime;
                            continue;
                        }
                    }

                    ulong i = k * k;

                    #if DETAILED_LOG
                        _console.WriteLine($"Number: {k} on thread: {Thread.CurrentThread.ManagedThreadId}");
                    #endif

                    while (i < _size)
                    {
                        _bits[i >> _shift] |= 1UL << (int)(i & _mask);
                        i += k;
                    }
                }
            }

            #if LOG
                _console.WriteLine("Finishing");
            #endif
        }

        public Task PrimesParallel()
        {
            return Task.Run(() =>
            {
                #if LOG
                    _console.WriteLine($"Task on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                #endif
                Primes();
            }
            );
        }

        public Task PrimesParallel(int number)
        {
            return Task.Run(() => 
            {
                #if LOG
                    _console.WriteLine($"Task number {number} on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
                #endif
                Primes();
            });
        }

        public Task PrimesNotParallel()
        {
            Primes();
            #if LOG
                _console.WriteLine($"Task on thread ID: {Thread.CurrentThread.ManagedThreadId}.");
            #endif
            return Task.CompletedTask;
        }

        public Task PrimesParallelSynchronized_1()
        {
            return Task.Run(() => PrimesSynchronized_1());
        }

        public Task PrimesParallelSynchronized_2()
        {
            return Task.Run(() => PrimesSynchronized_2());
        }

        public void PrintLargestPrime()
        {
            for (ulong i = _size - 1; i > 1; i--)
            {
                if ((_bits[i >> _shift] & (1UL << (int)(i & _mask))) == 0)
                {
                    _console.WriteLine($"Largest prime: {i}");
                    break;
                }
            }
        }

        public void PrintPrimes()
        {
            for (ulong i = 1; i < _size; i++)
            {
                if ((_bits[i >> _shift] & (1UL << (int)(i & _mask))) == 0)
                {
                    _console.WriteLine(i.ToString());
                }
            }
        }
    }
}
