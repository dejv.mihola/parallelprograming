﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Performance
{
    [MemoryDiagnoser]
    public class PerformanceTests
    {
        [Benchmark]
        public void PrimesBenchmark()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            eratosthenes.Primes();
        }

        [Benchmark]
        public async Task PrimesBenchmarkAsync()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            await eratosthenes.PrimesParallel();
        }

        [Benchmark]
        public async Task PrimesParallel()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            Task task1 = eratosthenes.PrimesParallel();
            Task task2 = eratosthenes.PrimesParallel();
            Task task3 = eratosthenes.PrimesParallel();
            Task task4 = eratosthenes.PrimesParallel();
            Task task5 = eratosthenes.PrimesParallel();

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;
        }

        [Benchmark]
        public async Task PrimesLooksParallel()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            Task task1 = eratosthenes.PrimesNotParallel();
            Task task2 = eratosthenes.PrimesNotParallel();
            Task task3 = eratosthenes.PrimesNotParallel();
            Task task4 = eratosthenes.PrimesNotParallel();
            Task task5 = eratosthenes.PrimesNotParallel();

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;
        }

        [Benchmark]
        public async Task PrimesNotParallel()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            await eratosthenes.PrimesNotParallel();
            await eratosthenes.PrimesNotParallel();
            await eratosthenes.PrimesNotParallel();
            await eratosthenes.PrimesNotParallel();
            await eratosthenes.PrimesNotParallel();
        }

        [Benchmark]
        public async Task PrimesParallelSynchronized_1()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            Task task1 = eratosthenes.PrimesParallelSynchronized_1();
            Task task2 = eratosthenes.PrimesParallelSynchronized_1();
            Task task3 = eratosthenes.PrimesParallelSynchronized_1();
            Task task4 = eratosthenes.PrimesParallelSynchronized_1();
            Task task5 = eratosthenes.PrimesParallelSynchronized_1();

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;
        }

        [Benchmark]
        public async Task PrimesParallelSynchronized_2()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            Task task1 = eratosthenes.PrimesParallelSynchronized_2();
            Task task2 = eratosthenes.PrimesParallelSynchronized_2();
            Task task3 = eratosthenes.PrimesParallelSynchronized_2();
            Task task4 = eratosthenes.PrimesParallelSynchronized_2();
            Task task5 = eratosthenes.PrimesParallelSynchronized_2();

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;
        }

        [Benchmark]
        public void NewThreadsSynchronized_1()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            Thread thread1 = new Thread(() => eratosthenes.PrimesSynchronized_1());
            Thread thread2 = new Thread(() => eratosthenes.PrimesSynchronized_1());
            Thread thread3 = new Thread(() => eratosthenes.PrimesSynchronized_1());
            Thread thread4 = new Thread(() => eratosthenes.PrimesSynchronized_1());
            Thread thread5 = new Thread(() => eratosthenes.PrimesSynchronized_1());

            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();
            thread5.Start();

            thread1.Join();
            thread2.Join();
            thread3.Join();
            thread4.Join();
            thread5.Join();
        }

        [Benchmark]
        public void ThreadPoolThreadsSynchronized_1()
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            CountdownEvent countdown = new CountdownEvent(5);

            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
            }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
            }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
            }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
            }));
            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                eratosthenes.PrimesSynchronized_1();
                countdown.Signal();
            }));

            countdown.Wait();
        }

        public static List<int> Params() => new List<int>() { 10, 15, 20, 25, 30, 35, 40 }; 
        //public static List<int> Params() => new List<int>() { }; 

        [Benchmark]
        [ArgumentsSource(nameof(Params))]
        public async Task PrimesParallelMultiThreadsSynchronized_1(int threads)
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            Task[] tasks = new Task[threads];
            for (int i = 0; i < threads; i++)
            {
                tasks[i] = eratosthenes.PrimesParallelSynchronized_1();
            }

            for (int i = 0; i < threads; i++)
            {
                await tasks[i];
            }
        }

        [Benchmark]
        [ArgumentsSource(nameof(Params))]
        public async Task PrimesParallelMultiThreadsSynchronized_2(int threads)
        {
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            Task[] tasks = new Task[threads];
            for (int i = 0; i < threads; i++)
            {
                tasks[i] = eratosthenes.PrimesParallelSynchronized_2();
            }

            for (int i = 0; i < threads; i++)
            {
                await tasks[i];
            }
        }
    }
}
