
using BenchmarkDotNet.Attributes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Performance;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Examples
{
    [MemoryDiagnoser]
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void PrimesBenchmark()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;

            eratosthenes.Primes();
            
            long finish = DateTime.Now.Ticks;

            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }

        [Benchmark]
        [TestMethod]
        public async Task PrimesBenchmarkAsync()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;
            
            await eratosthenes.PrimesParallel();

            long finish = DateTime.Now.Ticks;
            
            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }

        [Benchmark]
        [TestMethod]
        public async Task PrimesParallel()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;

            Task task1 = eratosthenes.PrimesParallel();
            if (LOG.logTests)
                Console.WriteLine("task1");

            Task task2 = eratosthenes.PrimesParallel();
            if (LOG.logTests)
                Console.WriteLine("task2");

            Task task3 = eratosthenes.PrimesParallel();
            if (LOG.logTests)
                Console.WriteLine("task3");

            Task task4 = eratosthenes.PrimesParallel();
            if (LOG.logTests)
                Console.WriteLine("task4");

            Task task5 = eratosthenes.PrimesParallel();
            if (LOG.logTests)
                Console.WriteLine("task5");

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;

            long finish = DateTime.Now.Ticks;

            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }

        [Benchmark]
        [TestMethod]
        public async Task PrimesLooksParallel()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;

            Task task1 = eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task1");

            Task task2 = eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task2");

            Task task3 = eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task3");

            Task task4 = eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task4");

            Task task5 = eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task5");

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;

            long finish = DateTime.Now.Ticks;

            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }

        [Benchmark]
        [TestMethod]
        public async Task PrimesNotParallel()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;

            await eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task1");
            
            await eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task2");

            await eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task3");

            await eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task4");

            await eratosthenes.PrimesNotParallel();
            if (LOG.logTests)
                Console.WriteLine("task5");

            long finish = DateTime.Now.Ticks;

            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }

        [Benchmark]
        [TestMethod]
        public async Task PrimesParallelSynchronizedWrong()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;

            Task task1 = eratosthenes.PrimesParallelSynchronizedWrong();
            if (LOG.logTests)
                Console.WriteLine("task1");

            Task task2 = eratosthenes.PrimesParallelSynchronizedWrong();
            if (LOG.logTests)
                Console.WriteLine("task2");

            Task task3 = eratosthenes.PrimesParallelSynchronizedWrong();
            if (LOG.logTests)
                Console.WriteLine("task3");

            Task task4 = eratosthenes.PrimesParallelSynchronizedWrong();
            if (LOG.logTests)
                Console.WriteLine("task4");

            Task task5 = eratosthenes.PrimesParallelSynchronizedWrong();
            if (LOG.logTests)
                Console.WriteLine("task5");

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;

            long finish = DateTime.Now.Ticks;

            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }

        [Benchmark]
        [TestMethod]
        public async Task PrimesParallelSynchronizedRight()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;

            Task task1 = eratosthenes.PrimesParallelSynchronizedRight();
            if (LOG.logTests)
                Console.WriteLine("task1");

            Task task2 = eratosthenes.PrimesParallelSynchronizedRight();
            if (LOG.logTests)
                Console.WriteLine("task2");

            Task task3 = eratosthenes.PrimesParallelSynchronizedRight();
            if (LOG.logTests)
                Console.WriteLine("task3");

            Task task4 = eratosthenes.PrimesParallelSynchronizedRight();
            if (LOG.logTests)
                Console.WriteLine("task4");

            Task task5 = eratosthenes.PrimesParallelSynchronizedRight();
            if (LOG.logTests)
                Console.WriteLine("task5");

            await task1;
            await task2;
            await task3;
            await task4;
            await task5;

            long finish = DateTime.Now.Ticks;
            
            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }

        [Benchmark]
        [TestMethod]
        public async Task PrimesParallelMaxThreads()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;

            Task[] tasks = new Task[7];
            for (int i = 0; i < 7; i++)
            {
                tasks[i] = eratosthenes.PrimesParallelSynchronizedRight();
            }

            for (int i = 0; i < 7; i++)
            {
                await tasks[i];
            }

            long finish = DateTime.Now.Ticks;

            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }

        [Benchmark]
        [TestMethod]
        public async Task PrimesParallelTooManyThreads()
        {
            if (LOG.logTests)
                Console.WriteLine($"Test starts in thread: {Thread.CurrentThread.ManagedThreadId}.");
            Eratosthenes eratosthenes = new Eratosthenes(100_000_000);

            long start = DateTime.Now.Ticks;

            Task[] tasks = new Task[25];
            for (int i = 0; i < 25; i++)
            {
                tasks[i] = eratosthenes.PrimesParallelSynchronizedRight();
            }

            for (int i = 0; i < 25; i++)
            {
                await tasks[i];
            }

            long finish = DateTime.Now.Ticks;

            eratosthenes.PrintLargestPrime();
            Console.WriteLine($"Duration: {(finish - start) / 10_000} ms");
        }
    }
}

