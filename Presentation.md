---
marp: true
theme: gaia
color: #000
colorSecondary: #333
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
paginate: false
---

<!-- Prikaz pro prevod do PDF v PowerShellu: .\marp.exe .\Presentation.md --pdf; .\Presentation.pdf -->

# <!-- _class: lead --> Základy paralelního programování

---

# Možnosti tvorby paralelního kódu
#### 1, Tvorba *Task*
```
public Task<int> CopyInput(int input)
{
    return Task<int>.Run(() => { return input; });
}
```

#### 2, Tvorba nových vláken
```
Thread thread = new Thread(() => eratosthenes.Primes());
thread.Start();
...
thread.Join();
```

---

# Možnosti tvorby paralelního kódu
#### 3, Využívání *ThreadPool*
```
CountdownEvent countdown = new CountdownEvent(1);
ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
{
    eratosthenes.Primes();
    countdown.Signal();
}));
...
countdown.Wait();
```

---

# Kde používat vlákna, kde Task v kombinaci s async-await?
* Výhradně používat **Task-async-await**, díky lepší čitelnosti kódu a podpoře v jazyce C#.
* Na některých místech to ale nemusí být možné/vhodné, např.
	* úlohy paralelně běžící po celou dobu běhu aplikace,
	* úlohy jejichž běh přesahuje jednu funkci,
	* úlohy nepodporující **Task-await-async**.

---

# Jak postupovat?
1.  Napast kód synchronně,
2.  změřit jeho výkonnost a najít pomalé úseky - *bottlenecks*
3.  zvážit, jestli by paralelizace měla velký vliv na urychlení,
4.  napsat paralelní kód,
5.  změřit výkonnost paralelního kódu.

---

<style scoped>
table {
    font-size: 25px;
}
</style>

# Synchronizace
Pomocí zámků:

|     Jméno/Způsob       |             Účel              | Podpora procesů | Zpoždění |
| :--------------------: | :---------------------------: | :-------------: | :------: |
|         `lock`         | Zabraňuje současnému přístupu |                 |   20ns   |
|        `Mutex`         | Zabraňuje současnému přístupu |        X        |  1000ns  |
|    `SemaphoreSlim`     |     Umožňuje N přístupů       |                 |  200ns   |
|      `Semaphore`       |     Umožňuje N přístupů       |        X        |  1000ns  |
| `ReaderWriterLockSlim` | Synchronizace čtení a zápisu  |                 |   40ns   |
|   `ReaderWriterLock`   | Synchronizace čtení a zápisu  |                 |  100ns   |

---

# Synchronizace
Použitím *thread-safe* datových struktur:
* ``BlockingCollection``
* ``ConcurrentDictionary``
* ``ConcurrentQueue``
* ``ConcurrentStack``
* ``ConcurrentBag``

---

# Synchronizace - problémy
* Deadlock (uváznutí)
* Data race (souběh nad daty)
* Blocking (blokování)
* Starvation (stárnutí)

---

# Co se bude dít?
#### Ukázka 1
```
HttpResponseMessage response = await client.GetAsync("http://google.com");
```
#### Ukázka 2
```
public Task Primes_1() {
    Primes();
    return Task.CompletedTask;
} ...
Task primes = Primes_1();
...
await primes;
```

---

# Co se bude dít?
#### Ukázka 3
```
public Task Primes_2() {
    return Task.Run(() => Primes());
} ...
Task primes = Primes_2();
...
await primes;
```

#### Ukázka 4
```
Primes_2().Wait();
HttpResponseMessage response = client.GetAsync("http://google.com").Result;
```

---

# Co se bude dít?
#### Ukázka 5
```
Task task1 = eratosthenes.Primes_2();
Task<HttpResponseMessage> task2 = client.GetAsync("http://google.com");

await task1;
HttpResponseMessage response = await task2;
```

---

# Benchmark - Eratosthenovo síto
* Bitové pole o požadované velikosti tvořené 64-bit integery
	* výpočet velikosti pole: ``((size - 1) >> shift) + 1``
	* vysvětlení:
		* ``size`` - požadovaný počet prozkoumaných čisel zadaný uživatelem (velikost bitového pole), 
		* ``shift`` - dělení ``sizeof(ulong) * 8 == 64`` -> dělení ``2^6`` -> bitový posun o ``6``,
		* ``+-1`` - zarovnání.

---

# Benchmark - Eratosthenovo síto
* Zjištění hodnoty n-tého bitu
	* vzorec: ``bits[n >> shift] & (1UL << (int)(n & mask))``
	* vysvětlení:
		* ``bits`` - bitové pole
		* ``n >> shift`` - výpočet umístění hledaného bitu v ``ulong[]``,
		* ``mask`` - modulo ``sizeof(ulong) * 8 == 64`` -> bitový and s ``64 - 1 == 63 == 0b0011_1111``,
		* ``1UL << ...`` - lokalizace konkrétního bitu v ``ulong``.

---

# Benchmark - Eratosthenovo síto
* Nastavení hodnoty n-tého bitu na 1, tedy **n** není prvočíslo
	* vzorec: ``bits[i >> shift] |= 1UL << (int)(i & mask)``,
	* vysvětlení viz slide předem, namísto bitového AND je použito bitové OR a rovnítko.

---

# Benchmark - Eratosthenovo síto
Synchronní kód:
```
for (ulong k = 2; k < _sqrt_size; k++)
{
    if ((_bits[k >> _shift] & (1UL << (int)(k & _mask))) == 0)
    {
        ulong i = k * k;
        while (i < _size)
        {
            _bits[i >> _shift] |= (1UL << (int)(i & _mask));
            i += k;
        }
    }
}
```

---

# Benchmark - Eratosthenovo síto
1. synchronizovaná verze pro možnost paralelizace
```
ulong i, k = 1;
while (k < _sqrt_size)
{
    lock (_lock)
    {
        for (k = _currentLargestPrime + 1; k < _sqrt_size; k++)
        {
            if (k > _currentLargestPrime && (_bits[k >> _shift] & (1UL << (int)(k & _mask))) == 0)
            {
                _currentLargestPrime = k;
                break;
            }
        }
    }
    ...
```

---

# Benchmark - Eratosthenovo síto
1. synchronizovaná verze pro možnost paralelizace

``` 
    ...
    i = k * k;
    while (i < _size)
    {
        _bits[i >> _shift] |= 1UL << (int)(i & _mask);
        i += k;
    }
}
```

---

# Benchmark - Eratosthenovo síto
2. synchronizovaná verze pro možnost paralelizace
```
for (ulong k = 2; k < _sqrt_size; k++)
{
    if (k > _currentLargestPrime && (_bits[k >> _shift] & (1UL << (int)(k & _mask))) == 0)
    {
        lock(_lock)
        {
            if (k > _currentLargestPrime)
                _currentLargestPrime = k;
            else
            {
                k = _currentLargestPrime;
                continue;
            }
        }
        ...
```

---

# Benchmark - Eratosthenovo síto
2. synchronizovaná verze pro možnost paralelizace
```     
        ...
        ulong i = k * k;
        while (i < _size)
        {
            _bits[i >> _shift] |= 1UL << (int)(i & _mask);
            i += k;
        }
    }
}
```
